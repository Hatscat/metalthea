## [0.4.4](https://gitlab.com/Hatscat/metalthea/compare/v0.4.3...v0.4.4) (2021-04-05)



## [0.4.3](https://gitlab.com/Hatscat/metalthea/compare/v0.4.2...v0.4.3) (2021-04-04)


### Bug Fixes

* replace postinstall by setup script ([c7a0917](https://gitlab.com/Hatscat/metalthea/commit/c7a09172d5928a688b615b7dc698af675e1c06d6))



## [0.4.2](https://gitlab.com/Hatscat/metalthea/compare/v0.4.1...v0.4.2) (2021-04-02)



## [0.4.1](https://gitlab.com/Hatscat/metalthea/compare/v0.4.0...v0.4.1) (2021-03-22)



# [0.4.0](https://gitlab.com/Hatscat/metalthea/compare/v0.3.0...v0.4.0) (2019-03-24)



# [0.3.0](https://gitlab.com/Hatscat/metalthea/compare/v0.2.1...v0.3.0) (2019-03-24)



## [0.2.1](https://gitlab.com/Hatscat/metalthea/compare/v0.2.0...v0.2.1) (2019-03-20)



# [0.2.0](https://gitlab.com/Hatscat/metalthea/compare/v0.1.3...v0.2.0) (2019-03-09)



## [0.1.3](https://gitlab.com/Hatscat/metalthea/compare/v0.1.2...v0.1.3) (2019-03-09)



## 0.1.2 (2019-03-08)



## 0.1.1 (2019-03-06)



# 0.1.0 (2019-03-06)




