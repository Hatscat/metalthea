import { parseProgram } from '../src/parser'
import { evalTree } from '../src/interpreter'
import { programCases } from './program-cases'
import { execContext, macroExecContext } from './execution-context'

describe('parseProgram() + evalTree()', () => {
  for (const programCase of programCases) {
    test(`Parse program: "${programCase.program}"`, () => {
      const tree = parseProgram(programCase.program)
      expect(tree).toEqual(programCase.tree)
    })
    test(`Eval program: "${programCase.program}"`, () => {
      const product = evalTree(programCase.tree, {
        ...execContext,
        ...macroExecContext,
      })
      expect(product.result).toEqual(programCase.product)
    })
  }
})
