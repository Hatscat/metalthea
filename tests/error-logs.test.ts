import { parseProgram } from '../src/parser'
import { evalTree } from '../src/interpreter'
import { execContext } from './execution-context'

describe('evalTree().errorLogs', () => {
  test(`No logs if no error`, () => {
    const result = evalTree(parseProgram('(add 1 12)'), execContext)
    expect(result.errorLogs.length).toBe(0)
  })

  test(`Unknown Atom format`, () => {
    const result = evalTree(parseProgram('(%% }} @@)'), execContext)
    expect(result.errorLogs.length).toBe(3)
    expect(result.errorLogs.every((l) => /Unknown Atom format/.test(l))).toBe(
      true
    )
  })

  test(`Unknown function`, () => {
    const result = evalTree(
      parseProgram(
        '((not_a_function 33 (Not.A.Function 42 73)) > notAFunction)'
      ),
      execContext
    )
    expect(result.errorLogs.length).toBe(3)
    expect(result.errorLogs.every((l) => /Unknown function/.test(l))).toBe(true)
  })

  test(`Error with function`, () => {
    const result = evalTree(parseProgram('(join false)'), execContext)
    expect(result.errorLogs.length).toBe(1)
    expect(result.errorLogs.every((l) => /Error with function/.test(l))).toBe(
      true
    )
  })
})
