import { compileSourceCode } from '../src'
import { execContext, macroExecContext } from './execution-context'

describe('compileSourceCode()', () => {
  test('complete example', () => {
    const program = 'before (add 1 2 3 && > js.double) after'
    const expectedTree = {
      l: '{before }',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'js.double',
              r: {
                l: 'add',
                r: {
                  l: '1',
                  r: {
                    l: '2',
                    r: {
                      l: '3',
                      r: {
                        l: '&&',
                        r: null,
                      },
                    },
                  },
                },
              },
            },
            r: {
              l: '{(add 1 2 3 && > js.double)}',
              r: null,
            },
          },
        },
        r: {
          l: '{ after}',
          r: null,
        },
      },
    }
    const expectedValue = 'before 2 * (6) after'
    const expectedLogs = ['Unknown Atom format, token: "&&".']

    const compilationResult = compileSourceCode(program, {
      ...execContext,
      ...macroExecContext,
    })
    expect(compilationResult.syntaxTree).toEqual(expectedTree)
    expect(compilationResult.value).toEqual(expectedValue)
    expect(compilationResult.errorLogs).toEqual(expectedLogs)
  })

  test('result filter: no function', () => {
    const program = '(1 #(add))'
    const compilationResult = compileSourceCode(program, execContext)
    expect(compilationResult.value).toEqual('1')
  })

  test('result filter: keep false and 0', () => {
    const program = '(0, false, {}, NONE)'
    const compilationResult = compileSourceCode(program, {})
    expect(compilationResult.value).toEqual('0false')
  })
})
