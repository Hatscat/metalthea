import { evalTree } from '../src/interpreter'
import { Token } from '../src/data'
import { execContext, macroExecContext } from './execution-context'

const context = { ...execContext, ...macroExecContext }

function evalToken(token: Token) {
  return evalTree({ l: token, r: null }, context).result[0]
}

describe('evalTree->token2Atom()', () => {
  test('Valid token2Atom', () => {
    expect(evalToken('')).toBe(undefined)
    expect(evalToken('42')).toBe(42)
    expect(evalToken('-0')).toBe(-0)
    expect(evalToken('0xA2')).toBe('0xA2')
    expect(evalToken('"Hello W."')).toBe('"Hello W."')
    expect(evalToken('"Hello \\"you\\""')).toBe('"Hello "you""')
    expect(evalToken('true')).toBe(true)
    expect(evalToken('false')).toBe(false)
    expect(evalToken(':key23')).toBe('key23')
    expect(evalToken('{n => n * n}')).toBe('n => n * n')
    expect(evalToken('{() => (\\{ n: 42 \\})}')).toBe('() => ({ n: 42 })')
    expect(evalToken('`raw data`')).toBe('raw data')
    expect(evalToken('`() => (\\` n: 42 \\`)`')).toBe('() => (` n: 42 `)')
    expect(evalToken('add')).toBe(undefined)
    expect(evalToken('js.mul')).toBe(undefined)
    expect(evalToken('#')).toBe(undefined)
    expect(evalToken('|')).toBe(undefined)
  })

  test('Invalid token2Atom', () => {
    // @ts-expect-error
    expect(evalToken(56)).toBe(undefined)
    expect(evalToken('-0.')).toBe(undefined)
    expect(evalToken('&add')).toBe(undefined)
    expect(evalToken('7add')).toBe(undefined)
    expect(evalToken('nothing.there.yet')).toBe(undefined)
  })
})
