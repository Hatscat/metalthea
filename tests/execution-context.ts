import { ContextFunction, ExecutionContext } from '../src/data'

export const execContext: ExecutionContext<ContextFunction> = {}

const globalData: Record<string, any> = {}

/** macro functions */
execContext.add = function (...arguments_) {
  return arguments_.reduce((a, b) => a + b)
}
execContext.exec = function (expr) {
  return eval(expr)
}
execContext.defLocal = function (variableName, value) {
  this[variableName] = value
}
execContext.defGlobal = function (variableName, value) {
  globalData[variableName] = value
  return ''
}
execContext.getLocal = function (variableName) {
  return this[variableName] as any
}
execContext.getGlobal = function (variableName) {
  return globalData[variableName]
}
execContext.Array = function (...arguments_) {
  return [...arguments_]
}
execContext.map = function (lambda) {
  return (array: unknown[]) =>
    array.map((element, index, array) => lambda(element, index, array))
}
execContext.reverse = function <T>(array: T[]) {
  return array.map((_: T, index: number) => array[array.length - index - 1])
}
execContext.sort = function (array) {
  return array.sort((a: number, b: number) => a - b)
}
execContext.head = function (array) {
  return array[0]
}
execContext.for = function (from, to, step, bodyLambda) {
  let result: any[] = []
  for (let index = from; index < to; index += step) {
    result = [...result, bodyLambda(index, from, to, step)]
  }
  return result
}
execContext.join = function <T>(array: T[], separator: string) {
  return array.join(separator || '')
}

/** javascript code output functions */
export const macroExecContext: ExecutionContext<ExecutionContext> = { js: {} }

macroExecContext.js.add = function (...arguments_) {
  return arguments_.reduce((a, b) => `${a} + ${b}`)
}
macroExecContext.js.mul = function (...arguments_) {
  return arguments_.reduce((a, b) => `${a} * ${b}`)
}
macroExecContext.js.double = function (n) {
  return `2 * (${n})`
}
macroExecContext.js.sqrt = function (n) {
  return `Math.sqrt(${n})`
}
macroExecContext.js.log = function (...arguments_) {
  return `console.log(${arguments_.join(',')});\n`
}
