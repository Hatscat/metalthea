#!/usr/bin/env bash

tests_dir="tests/integration"

rm -r "$tests_dir/result";

node dist/cli.js "$tests_dir/ctx_mock/*.js" "$tests_dir/src_mock" "$tests_dir/result" \
&& diff -r "$tests_dir/result" "$tests_dir/expected_result" \
&& echo 'Integration Test passed!';
