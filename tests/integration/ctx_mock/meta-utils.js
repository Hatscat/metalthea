/** macro functions */
const execContext = {}

const globalData = {}

execContext.add = function (...arguments_) {
  return arguments_.reduce((a, b) => a + b)
}
execContext.sub = function (...arguments_) {
  return arguments_.reduce((a, b) => a - b)
}
execContext.mul = function (...arguments_) {
  return arguments_.reduce((a, b) => a * b)
}
execContext.div = function (...arguments_) {
  return arguments_.reduce((a, b) => a / b)
}
execContext.mod = function (...arguments_) {
  return arguments_.reduce((a, b) => a % b)
}
execContext.exec = function (expr) {
  return eval(expr)
}
execContext.defLocal = function (variableName, value) {
  this[variableName] = value
}
execContext.defGlobal = function (variableName, value) {
  globalData[variableName] = value
  return ''
}
execContext.getLocal = function (variableName) {
  return this[variableName]
}
execContext.getGlobal = function (variableName) {
  return globalData[variableName]
}
execContext.Record = function (...arguments_) {
  const record = {}
  for (let index = 0; index < arguments_.length; index += 2) {
    record[arguments_[index]] = arguments_[index + 1]
  }
  return record
}
execContext.Array = function (...arguments_) {
  return [...arguments_]
}
execContext.map = function (lambda) {
  return (array) =>
    array.map((element, index, array) => lambda(element, index, array))
}
execContext.reverse = function (array) {
  return array.map((_, index) => array[array.length - index - 1])
}
execContext.sort = function (array) {
  return array.sort((a, b) => a - b)
}
execContext.head = function (array) {
  return array[0]
}
execContext.for = function (from, to, step, bodyLambda) {
  let result = []
  for (let index = from; index < to; index += step) {
    result = [...result, bodyLambda(index, from, to, step)]
  }
  return result
}
execContext.join = function (array, separator) {
  return array.join(separator || '')
}

module.exports = { m: execContext }
