/** javascript code output functions */
const execContext = {}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const globalData = {}

execContext.add = function (...arguments_) {
  return arguments_.reduce((a, b) => `${a} + ${b}`)
}
execContext.sub = function (...arguments_) {
  return arguments_.reduce((a, b) => `${a} - ${b}`)
}
execContext.mul = function (...arguments_) {
  return arguments_.reduce((a, b) => `${a} * ${b}`)
}
execContext.div = function (...arguments_) {
  return arguments_.reduce((a, b) => `${a} / ${b}`)
}
execContext.mod = function (...arguments_) {
  return arguments_.reduce((a, b) => `${a} % ${b}`)
}
execContext.double = function (n) {
  return `2 * (${n})`
}
execContext.sqrt = function (n) {
  return `Math.sqrt(${n})`
}
execContext.Record = function (...arguments_) {
  return JSON.stringify(execContext.Record(...arguments_))
}
execContext.Array = function (...arguments_) {
  return `[${arguments_.join(',')}]`
}
execContext.map = function (lambda) {
  return (array) => `${array}.map(${lambda})`
}
execContext.reverse = function (array) {
  return `${array}.map((_, i, a) => a[a.length - i - 1])`
}
execContext.sort = function (array) {
  return `${array}.sort((a, b) => a - b)`
}
execContext.head = function (array) {
  return `${array}[0]`
}
execContext.log = function (...arguments_) {
  return `console.log(${arguments_.join(',')});\n`
}
execContext.logThis = function () {
  return execContext.log(JSON.stringify(this))
}

module.exports = execContext
