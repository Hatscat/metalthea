import { TreeNode } from '../src/data'

export const programCases: Array<{
  program: string
  tree: TreeNode
  product: (string | number | Array<string | number>)[]
}> = [
  {
    program: '',
    tree: { l: '{}', r: null },
    product: [''],
  },
  {
    program: ' ( add  2, 4 )  ',
    tree: {
      l: '{ }',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'add',
              r: { l: '2', r: { l: '4', r: null } },
            },
            r: {
              l: '{( add  2, 4 )}',
              r: null,
            },
          },
        },
        r: {
          l: '{  }',
          r: null,
        },
      },
    },
    product: [' ', 6, '  '],
  },
  {
    program: `
(js.mul
  11
  ( add 2, 3)
  4)
`,
    tree: {
      l: '{\n}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'js.mul',
              r: {
                l: {
                  l: '11',
                  r: {
                    l: 'add',
                    r: { l: '2', r: { l: '3', r: null } },
                  },
                },
                r: { l: '4', r: null },
              },
            },
            r: {
              l: '{(js.mul\n  11\n  ( add 2, 3)\n  4)}',
              r: null,
            },
          },
        },
        r: {
          l: '{\n}',
          r: null,
        },
      },
    },
    product: ['\n', '11 * 5 * 4', '\n'],
  },
  {
    program: '((4 8) > js.mul)',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'js.mul',
              r: { l: { l: '4', r: { l: '8', r: null } }, r: null },
            },
            r: {
              l: '{((4 8) > js.mul)}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', '4 * 8', ''],
  },
  {
    program: '( 8,4,2 > js.mul > js.double > js.sqrt )',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'js.sqrt',
              r: {
                l: 'js.double',
                r: {
                  l: 'js.mul',
                  r: { l: '8', r: { l: '4', r: { l: '2', r: null } } },
                },
              },
            },
            r: {
              l: '{( 8,4,2 > js.mul > js.double > js.sqrt )}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', 'Math.sqrt(2 * (8 * 4 * 2))', ''],
  },
  {
    program: '((defLocal :n 42) (getLocal :n))',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: {
                l: 'defLocal',
                r: {
                  l: ':n',
                  r: {
                    l: '42',
                    r: null,
                  },
                },
              },
              r: {
                l: 'getLocal',
                r: {
                  l: ':n',
                  r: null,
                },
              },
            },
            r: {
              l: '{((defLocal :n 42) (getLocal :n))}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', 42, ''],
  },
  {
    program: '(defGlobal :n 42) (getGlobal :n)',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'defGlobal',
              r: {
                l: ':n',
                r: {
                  l: '42',
                  r: null,
                },
              },
            },
            r: {
              l: '{(defGlobal :n 42)}',
              r: null,
            },
          },
        },
        r: {
          l: '{ }',
          r: {
            l: {
              l: '|',
              r: {
                l: {
                  l: 'getGlobal',
                  r: {
                    l: ':n',
                    r: null,
                  },
                },
                r: {
                  l: '{(getGlobal :n)}',
                  r: null,
                },
              },
            },
            r: {
              l: '{}',
              r: null,
            },
          },
        },
      },
    },
    product: ['', '', ' ', 42, ''],
  },
  {
    program: '((map (exec {(n, i) => (i+"\\} "+n)})) (Array 1 2 3))',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: {
                l: 'map',
                r: {
                  l: 'exec',
                  r: {
                    l: '{(n, i) => (i+"\\} "+n)}',
                    r: null,
                  },
                },
              },
              r: {
                l: 'Array',
                r: {
                  l: '1',
                  r: {
                    l: '2',
                    r: {
                      l: '3',
                      r: null,
                    },
                  },
                },
              },
            },
            r: {
              l: '{((map (exec \\{(n, i) => (i+"\\} "+n)\\})) (Array 1 2 3))}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', ['0} 1', '1} 2', '2} 3'], ''],
  },
  {
    program: '(js.log "Hello, world!", "(#{\\"})")',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'js.log',
              r: {
                l: '"Hello, world!"',
                r: {
                  l: '"(#{\\"})"',
                  r: null,
                },
              },
            },
            r: {
              l: '{(js.log "Hello, world!", "(#\\{\\"\\})")}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', 'console.log("Hello, world!","(#{"})");\n', ''],
  },
  {
    program: '(()({})(42))',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: {
                l: {
                  l: '',
                  r: null,
                },
                r: {
                  l: '{}',
                  r: null,
                },
              },
              r: {
                l: '42',
                r: null,
              },
            },
            r: {
              l: '{(()(\\{\\})(42))}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', '', 42, ''],
  },
  {
    program: `(
(defLocal :myArr (Array 4 12 3 7))
(defGlobal :myVar (
  (getLocal :myArr)
  > (map (exec {n => n * n}))
  > sort
  > reverse
  > head)))
(getGlobal :myVar)`,
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: {
                l: 'defLocal',
                r: {
                  l: ':myArr',
                  r: {
                    l: 'Array',
                    r: {
                      l: '4',
                      r: {
                        l: '12',
                        r: {
                          l: '3',
                          r: {
                            l: '7',
                            r: null,
                          },
                        },
                      },
                    },
                  },
                },
              },
              r: {
                l: 'defGlobal',
                r: {
                  l: ':myVar',
                  r: {
                    l: 'head',
                    r: {
                      l: 'reverse',
                      r: {
                        l: 'sort',
                        r: {
                          l: {
                            l: 'map',
                            r: {
                              l: 'exec',
                              r: {
                                l: '{n => n * n}',
                                r: null,
                              },
                            },
                          },
                          r: {
                            l: {
                              l: 'getLocal',
                              r: {
                                l: ':myArr',
                                r: null,
                              },
                            },
                            r: null,
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
            r: {
              l:
                '{(\n(defLocal :myArr (Array 4 12 3 7))\n(defGlobal :myVar (\n  (getLocal :myArr)\n  > (map (exec \\{n => n * n\\}))\n  > sort\n  > reverse\n  > head)))}',
              r: null,
            },
          },
        },
        r: {
          l: '{\n}',
          r: {
            l: {
              l: '|',
              r: {
                l: {
                  l: 'getGlobal',
                  r: {
                    l: ':myVar',
                    r: null,
                  },
                },
                r: {
                  l: '{(getGlobal :myVar)}',
                  r: null,
                },
              },
            },
            r: {
              l: '{}',
              r: null,
            },
          },
        },
      },
    },
    product: ['', '', '\n', 144, ''],
  },
  {
    program: '(for 0,3,1 #(js.log) > join)',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'join',
              r: {
                l: 'for',
                r: {
                  l: '0',
                  r: {
                    l: '3',
                    r: {
                      l: '1',
                      r: {
                        l: '#',
                        r: {
                          l: 'js.log',
                          r: null,
                        },
                      },
                    },
                  },
                },
              },
            },
            r: {
              l: '{(for 0,3,1 #(js.log) > join)}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: [
      '',
      'console.log(0,0,3,1);\nconsole.log(1,0,3,1);\nconsole.log(2,0,3,1);\n',
      '',
    ],
  },
  {
    program: '#before (add 1 11) after',
    tree: {
      l: '{#before }',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: 'add',
              r: {
                l: '1',
                r: {
                  l: '11',
                  r: null,
                },
              },
            },
            r: {
              l: '{(add 1 11)}',
              r: null,
            },
          },
        },
        r: {
          l: '{ after}',
          r: null,
        },
      },
    },
    product: ['#before ', 12, ' after'],
  },
  {
    program: ' (%% }} @@) ',
    tree: {
      l: '{ }',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: '%%',
              r: {
                l: '}}',
                r: {
                  l: '@@',
                  r: null,
                },
              },
            },
            r: {
              l: '{(%% \\}\\} @@)}',
              r: null,
            },
          },
        },
        r: {
          l: '{ }',
          r: null,
        },
      },
    },
    product: [' ', '(%% }} @@)', ' '],
  },
  {
    program: 'let a = () => ({n: 42});',
    tree: {
      l: '{let a = }',
      r: {
        l: {
          l: '|',
          r: {
            l: { l: '', r: null },
            r: {
              l: '{()}',
              r: null,
            },
          },
        },
        r: {
          l: '{ => }',
          r: {
            l: {
              l: '|',
              r: {
                l: {
                  l: '{n: 42}',
                  r: null,
                },
                r: {
                  l: '{(\\{n: 42\\})}',
                  r: null,
                },
              },
            },
            r: {
              l: '{;}',
              r: null,
            },
          },
        },
      },
    },
    product: ['let a = ', '()', ' => ', 'n: 42', ';'],
  },
  {
    program: '({let a = () => (\\{n: 42\\});})',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: '{let a = () => (\\{n: 42\\});}',
              r: null,
            },
            r: {
              l: '{(\\{let a = () => (\\{n: 42\\});\\})}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', 'let a = () => ({n: 42});', ''],
  },
  {
    program: 'let a = \\(\\) => \\({n: 42}\\);',
    tree: {
      l: '{let a = \\(\\) => \\(\\{n: 42\\}\\);}',
      r: null,
    },
    product: ['let a = () => ({n: 42});'],
  },
  {
    program: '(#)',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: '#',
              r: null,
            },
            r: {
              l: '{(#)}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', '(#)', ''],
  },
  {
    program: '((js.log "Hi") {\\n} (js.log "Bye"))',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: {
                l: 'js.log',
                r: {
                  l: '"Hi"',
                  r: null,
                },
              },
              r: {
                l: '{\\n}',
                r: {
                  l: 'js.log',
                  r: {
                    l: '"Bye"',
                    r: null,
                  },
                },
              },
            },
            r: {
              l: '{((js.log "Hi") \\{\\n\\} (js.log "Bye"))}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', 'console.log("Hi");\n', '\n', 'console.log("Bye");\n', ''],
  },
  {
    program: '({Hello} `\\tWorld !`)',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: '{Hello}',
              r: {
                l: '`\\tWorld !`',
                r: null,
              },
            },
            r: {
              l: '{(\\{Hello\\} \\`\\tWorld !\\`)}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', 'Hello', '\tWorld !', ''],
  },
  {
    program: '(#(add) 1 2 3 4)',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: {
                l: '#',
                r: {
                  l: 'add',
                  r: null,
                },
              },
              r: {
                l: '1',
                r: {
                  l: '2',
                  r: {
                    l: '3',
                    r: {
                      l: '4',
                      r: null,
                    },
                  },
                },
              },
            },
            r: {
              l: '{(#(add) 1 2 3 4)}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', 10, ''],
  },
  {
    program: '(1 2 3 4 > ( #(js.add > js.sqrt > (#js.mul 3)) ))',
    tree: {
      l: '{}',
      r: {
        l: {
          l: '|',
          r: {
            l: {
              l: {
                l: '#',
                r: {
                  l: {
                    l: '#',
                    r: {
                      l: 'js.mul',
                      r: {
                        l: '3',
                        r: null,
                      },
                    },
                  },
                  r: {
                    l: 'js.sqrt',
                    r: {
                      l: 'js.add',
                      r: null,
                    },
                  },
                },
              },
              r: {
                l: '1',
                r: {
                  l: '2',
                  r: {
                    l: '3',
                    r: {
                      l: '4',
                      r: null,
                    },
                  },
                },
              },
            },
            r: {
              l: '{(1 2 3 4 > ( #(js.add > js.sqrt > (#js.mul 3)) ))}',
              r: null,
            },
          },
        },
        r: {
          l: '{}',
          r: null,
        },
      },
    },
    product: ['', '3 * Math.sqrt(1 + 2 + 3 + 4)', ''],
  },
]
