import * as grammar from '../src/grammar'

describe('grammar', () => {
  test('Valid NUMBER_B10', () => {
    expect(grammar.NUMBER_B10.test('42')).toBe(true)
    expect(grammar.NUMBER_B10.test('-137')).toBe(true)
    expect(grammar.NUMBER_B10.test('0')).toBe(true)
    expect(grammar.NUMBER_B10.test('-0')).toBe(true)
    expect(grammar.NUMBER_B10.test('0.4568732')).toBe(true)
    expect(grammar.NUMBER_B10.test('-7.7786399')).toBe(true)
    expect(grammar.NUMBER_B10.test('-0123456789.0123456789')).toBe(true)
  })

  test('Invalid NUMBER_B10', () => {
    expect(grammar.NUMBER_B10.test('A42')).toBe(false)
    expect(grammar.NUMBER_B10.test('(2)')).toBe(false)
    expect(grammar.NUMBER_B10.test('.4')).toBe(false)
    expect(grammar.NUMBER_B10.test('7.')).toBe(false)
    expect(grammar.NUMBER_B10.test('8.12.')).toBe(false)
    expect(grammar.NUMBER_B10.test('5.43.69')).toBe(false)
  })

  test('Valid NUMBER_B16', () => {
    expect(grammar.NUMBER_B16.test('0xA42')).toBe(true)
    expect(grammar.NUMBER_B16.test('0x007')).toBe(true)
    expect(grammar.NUMBER_B16.test('0xFEDCBA9876543210')).toBe(true)
    expect(grammar.NUMBER_B16.test('0xfedcba.fedcba')).toBe(true)
    expect(grammar.NUMBER_B16.test('-0xFEDCBA9876543210')).toBe(true)
    expect(grammar.NUMBER_B16.test('-0xFEDCBA98.76543210')).toBe(true)
    expect(grammar.NUMBER_B16.test('0xFEDCBA98.76543210')).toBe(true)
    expect(grammar.NUMBER_B16.test('-0x00.000')).toBe(true)
  })

  test('Invalid NUMBER_B16', () => {
    expect(grammar.NUMBER_B16.test('A42')).toBe(false)
    expect(grammar.NUMBER_B16.test('0Ax69')).toBe(false)
    expect(grammar.NUMBER_B16.test('0Cx72')).toBe(false)
  })

  test('Valid STRING', () => {
    expect(grammar.STRING.test('""')).toBe(true)
    expect(grammar.STRING.test('"aZ0.?#(u{T})!*"')).toBe(true)
    expect(grammar.STRING.test('"\\"([azr])\\""')).toBe(true)
  })

  test('Invalid STRING', () => {
    expect(grammar.STRING.test('"')).toBe(false)
    expect(grammar.STRING.test('"aZ0.?#(u{T})!*')).toBe(false)
    expect(grammar.STRING.test('"aze"rty')).toBe(false)
  })

  test('Valid BOOL', () => {
    expect(grammar.BOOL.test('true')).toBe(true)
    expect(grammar.BOOL.test('false')).toBe(true)
  })

  test('Invalid BOOL', () => {
    expect(grammar.BOOL.test('"true"')).toBe(false)
    expect(grammar.BOOL.test('TRUE')).toBe(false)
    expect(grammar.BOOL.test('FALSE')).toBe(false)
    expect(grammar.BOOL.test('yes')).toBe(false)
  })

  test('Valid KEY', () => {
    expect(grammar.KEY.test(':tRue')).toBe(true)
    expect(grammar.KEY.test(':_$_$')).toBe(true)
    expect(grammar.KEY.test(':AzErT0153')).toBe(true)
  })

  test('Invalid KEY', () => {
    expect(grammar.KEY.test('AzErT0153')).toBe(false)
    expect(grammar.KEY.test(':-8')).toBe(false)
    expect(grammar.KEY.test(':a.?')).toBe(false)
  })

  test('Valid BODY', () => {
    expect(grammar.BODY.test('{}')).toBe(true)
    expect(grammar.BODY.test('``')).toBe(true)
    expect(grammar.BODY.test('{9 a.?#([-])$}')).toBe(true)
    expect(grammar.BODY.test('{"\\}z}')).toBe(true)
    expect(grammar.BODY.test('{9 \\{a.?#([-]\\})$}')).toBe(true)
    expect(grammar.BODY.test('`9 \\`a.?#([-]\\`)$`')).toBe(true)
  })

  test('Invalid BODY', () => {
    expect(grammar.BODY.test('"{a}"')).toBe(false)
    expect(grammar.BODY.test('"`42`"')).toBe(false)
    expect(grammar.BODY.test('{az')).toBe(false)
    expect(grammar.BODY.test('RT`')).toBe(false)
  })

  test('Valid FUNCTION', () => {
    expect(grammar.FUNCTION.test('aZ25x')).toBe(true)
    expect(grammar.FUNCTION.test('D')).toBe(true)
    expect(grammar.FUNCTION.test('r.r')).toBe(true)
    expect(grammar.FUNCTION.test('a.z1.f.r8')).toBe(true)
  })

  test('Invalid FUNCTION', () => {
    expect(grammar.FUNCTION.test('5a')).toBe(false)
    expect(grammar.FUNCTION.test('a.')).toBe(false)
    expect(grammar.FUNCTION.test('b.5')).toBe(false)
    expect(grammar.FUNCTION.test('.b.c')).toBe(false)
  })

  test('Valid LAZY_NODE', () => {
    expect(grammar.LAZY_NODE.test('#')).toBe(true)
  })

  test('Invalid LAZY_NODE', () => {
    expect(grammar.LAZY_NODE.test('"#"')).toBe(false)
    expect(grammar.LAZY_NODE.test('##')).toBe(false)
    expect(grammar.LAZY_NODE.test('#a')).toBe(false)
  })

  test('Valid OR_NODE', () => {
    expect(grammar.OR_NODE.test('|')).toBe(true)
  })

  test('Invalid OR_NODE', () => {
    expect(grammar.OR_NODE.test('"|"')).toBe(false)
    expect(grammar.OR_NODE.test('||')).toBe(false)
    expect(grammar.OR_NODE.test('|a')).toBe(false)
  })
})
