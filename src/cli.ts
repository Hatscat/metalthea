#!/usr/bin/env node

import meow from 'meow'
import globby from 'globby'
import makeDir from 'make-dir'
import chokidar from 'chokidar'
import fs from 'fs'
import { compileSourceCode, CompilationResult } from './index'
import { ExecutionContext } from './data'

const log = console.log.bind(console)
const logError = console.error.bind(console)

const cli = meow(
  `
    Usage
      $ metalthea <CTX> <SRC> <TARGET>

    Options
      --verbose, -v  add logs to stdout
      --syntax-tree, -t  logs the intermediate syntax tree in stdout
      --watch, -w  watch context and source files and re-compile if any change is detected

    Examples
      $ metalthea src/metalthea src/js dist --verbose --watch
  `,
  {
    flags: {
      verbose: {
        type: 'boolean',
        alias: 'v',
      },
      syntaxTree: {
        type: 'boolean',
        alias: 't',
      },
      watch: {
        type: 'boolean',
        alias: 'w',
      },
    },
  }
)

const globbyOptions = { absolute: true } as const
const watcherOptions = { ignoreInitial: true, cwd: '.' } as const

;(async () => {
  if (cli.input.length !== 3) {
    cli.showHelp(1)
  }
  const [contextPath, sourcePath, targetPath] = cli.input

  const [
    contextFilePaths,
    sourceFilePaths,
    targetDirectoryPath,
  ] = await Promise.all([
    globby(contextPath, globbyOptions),
    globby(sourcePath, globbyOptions),
    makeDir(targetPath),
  ])

  if (cli.flags.watch) {
    // context files: recompile everything
    chokidar
      .watch(contextPath, watcherOptions)
      .on('add', async (path) => {
        contextFilePaths.push(...(await globby(path, globbyOptions)))
        compile(contextFilePaths, sourceFilePaths, targetDirectoryPath)
      })
      .on('change', () =>
        compile(contextFilePaths, sourceFilePaths, targetDirectoryPath)
      )
      .on('unlink', handleWatchUnlink(contextFilePaths))
    // source files: compile only related files
    chokidar
      .watch(sourcePath, watcherOptions)
      .on('add', async (path) => {
        const addedSourceFilePaths = await globby(path, globbyOptions)
        sourceFilePaths.push(...addedSourceFilePaths)
        compile(contextFilePaths, addedSourceFilePaths, targetDirectoryPath)
      })
      .on('change', async (path) => {
        const updatedSourceFilePaths = await globby(path, globbyOptions)
        compile(contextFilePaths, updatedSourceFilePaths, targetDirectoryPath)
      })
      .on('unlink', handleWatchUnlink(sourceFilePaths))
  }

  await compile(contextFilePaths, sourceFilePaths, targetDirectoryPath)
})()

function handleWatchUnlink(pathList: string[]): (path: string) => void {
  return (path) => {
    const index = pathList.findIndex((p) => new RegExp(path).test(p))
    if (index !== -1) pathList.splice(index, 1)
  }
}

async function compile(
  contextFilePaths: string[],
  sourceFilePaths: string[],
  targetDirectoryPath: string
) {
  const context = composeContext(contextFilePaths)

  Promise.all(
    sourceFilePaths.map((filePath) =>
      computeSourceFile(context, filePath, targetDirectoryPath)
    )
  )
    .then(() => log('All files have been processed successfully.'))
    .catch((error) => logError('ERROR:', error))
}

function composeContext(contextFilePaths: string[]): ExecutionContext {
  return contextFilePaths.reduce((context_: ExecutionContext, path: string) => {
    delete require.cache[require.resolve(path)]
    let module_
    try {
      module_ = require(path)
    } catch (error) {
      logError(`ERROR while "require(${path})":\n`, error)
      process.exit(1)
    }
    return Object.assign(context_, module_)
  }, {})
}

async function computeSourceFile(
  context: ExecutionContext,
  filePath: string,
  targetDirectoryPath: string
) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, 'utf8', (error, sourceCode) => {
      if (error) return reject(error)
      try {
        const fileName = filePath.match(/[^/]+$/)?.[0] ?? 'index'
        const compilationResult: CompilationResult = compileSourceCode(
          sourceCode,
          context
        )
        log(`Successful Compilation for the file "${fileName}"`)
        if (cli.flags.syntaxTree) {
          log(JSON.stringify(compilationResult.syntaxTree))
        }
        if (cli.flags.verbose) {
          log(compilationResult.errorLogs.join('\n'))
        }
        writeFile(
          targetDirectoryPath,
          fileName,
          compilationResult.value,
          resolve
        )
      } catch (error) {
        return reject(error)
      }
    })
  })
}

function writeFile(
  directoryPath: string,
  fileName: string,
  value: string,
  resolve: (_: unknown) => void
) {
  fs.writeFile(`${directoryPath}/${fileName}`, value, (error_) => {
    if (error_) {
      ;(async () => {
        const outputPath = await makeDir(directoryPath)
        fs.writeFile(`${outputPath}/${fileName}`, value, resolve)
      })()
    } else {
      resolve(true)
    }
  })
}
