import { parseProgram } from './parser'
import { evalTree, EvalProduct } from './interpreter'
import { ExecutionContext, TreeNode } from './data'

export * as Data from './data'

export type CompilationResult = {
  value: string
  syntaxTree: TreeNode
  errorLogs: string[]
}

/**
 * Compile a source code containing Metalthea program(s) and return the result with logs.
 * @param {string} sourceCode the full source code from one file
 * @param {ExecutionContext} executionContext the combined execution context
 */
export function compileSourceCode(
  sourceCode: string,
  executionContext: ExecutionContext<any>
): CompilationResult {
  const tree: TreeNode = parseProgram(sourceCode)
  const product: EvalProduct = evalTree(tree, executionContext)

  return {
    value: product.result
      .filter((atom) => atom != null && typeof atom !== 'function')
      .join(''),
    syntaxTree: tree,
    errorLogs: product.errorLogs,
  }
}
