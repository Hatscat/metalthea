import { ContextFunction } from './execution-context'

export type Atom = string | number | boolean | RegExp | ContextFunction
