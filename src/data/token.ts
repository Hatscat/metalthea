export enum Op {
  ProgramStart = '(',
  ProgramEnd = ')',
  BodyStart = '{',
  BodyEnd = '}',
  KeyStart = ':',
  LazyNodeStart = '#',
  OrBranch = '|',
  Chain = '>',
  Escape = '\\',
  Quote = '"',
}

export type Token = string
