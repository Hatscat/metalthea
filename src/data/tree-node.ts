import { Atom } from './atom'
import { Op, Token } from './token'

export interface TreeNode {
  l: TreeNode | Token
  r: TreeNode | null
}

export type NodeProduct = Atom[] | null

export enum NodeSide {
  L = 'l',
  R = 'r',
}

export function createNode(isText?: boolean): TreeNode {
  return {
    l: isText ? Op.BodyStart : '',
    r: null,
  }
}

export function createOrNode() {
  return {
    l: Op.OrBranch,
    r: {
      l: '',
      r: createNode(true),
    },
  }
}
