export type ContextFunction = (...arguments_: any[]) => any

export type ExecutionContext<
  T extends ExecutionContext | ContextFunction = ContextFunction
> = { [key: string]: T }
