import { createNode, createOrNode, NodeSide, Op, TreeNode } from './data'

interface NodeWritingPointers {
  orRoot: TreeNode
  text: TreeNode
  prog: TreeNode
  progSide: NodeSide
}

const SEPARATOR_REGEX: RegExp = /\s|,/

function branchRight(currentNode: TreeNode, isText?: boolean): TreeNode {
  if (currentNode.r != null) {
    currentNode.l = Object.assign({}, currentNode)
  }
  currentNode.r = createNode(isText)
  return currentNode.r
}

function branchLeft(currentNode: TreeNode): TreeNode {
  currentNode.r = Object.assign({}, currentNode)
  currentNode.l = ''
  return currentNode
}

function programModeOn(currentNodes: NodeWritingPointers) {
  if (typeof currentNodes.text.l === 'string') {
    currentNodes.text.l += Op.BodyEnd
  }
  const newBranch = createOrNode()
  currentNodes.text.r = newBranch
  currentNodes.orRoot = newBranch
  currentNodes.prog = newBranch.r
  currentNodes.text = newBranch.r.r
  currentNodes.progSide = NodeSide.L
}

function programModeOff(currentNodes: NodeWritingPointers) {
  if (typeof currentNodes.text.l === 'string') {
    currentNodes.text.l += Op.BodyEnd
  }
  currentNodes.text = branchRight(currentNodes.orRoot, true)
}

function addChrInProgNode(
  currentNodes: NodeWritingPointers,
  chr: string
): void {
  if (typeof currentNodes.prog.l === 'string') currentNodes.prog.l += chr
}

/** parse Metalthea program string to nodes tree
 * @param {string} program The Metalthea program in string format.
 * @returns {TreeNode} The produced tree of Nodes.
 */
export function parseProgram(program: string = ''): TreeNode {
  const tree: TreeNode = createNode(true)
  let nodesStack: TreeNode[] = [tree]
  const parIndexStack: number[] = []
  let nextChrEscaped: boolean = false
  let programMode: boolean = false
  let bodyOpen: boolean = false
  let quotesOpen: boolean = false

  const currentNodes: NodeWritingPointers = {
    orRoot: tree,
    text: tree,
    prog: tree,
    progSide: NodeSide.L,
  }

  for (const chr of program) {
    if (!programMode && !nextChrEscaped && chr === Op.ProgramStart) {
      programModeOn(currentNodes)
      programMode = true
    }

    if (typeof currentNodes.text.l === 'string') {
      if (!nextChrEscaped && /[`{}]/.test(chr)) {
        currentNodes.text.l += Op.Escape
      }
      currentNodes.text.l += chr
    }

    if (!programMode) {
      nextChrEscaped = chr === Op.Escape
      continue
    }

    if (bodyOpen || quotesOpen) {
      if (nextChrEscaped) {
        nextChrEscaped = false
        addChrInProgNode(currentNodes, chr)
        continue
      }
      nextChrEscaped = chr === Op.Escape

      if (bodyOpen) {
        bodyOpen = !/[`}]/.test(chr)
      } else if (quotesOpen) {
        quotesOpen = chr !== Op.Quote
      }

      addChrInProgNode(currentNodes, chr)
      continue
    }

    switch (chr) {
      case Op.ProgramStart: {
        parIndexStack.push(nodesStack.length)
        if (currentNodes.progSide === NodeSide.L) {
          currentNodes.prog.l = createNode(false)
          currentNodes.prog = currentNodes.prog.l
          nodesStack.push(currentNodes.prog)
        } else if (currentNodes.progSide === NodeSide.R) {
          currentNodes.progSide = NodeSide.L
          currentNodes.prog = branchRight(currentNodes.prog)
          nodesStack.push(currentNodes.prog)
        }
        break
      }
      case Op.ProgramEnd: {
        nodesStack = nodesStack.slice(0, parIndexStack.pop())
        currentNodes.prog = nodesStack[nodesStack.length - 1]
        currentNodes.progSide = NodeSide.R

        if (parIndexStack.length === 0) {
          programModeOff(currentNodes)
          programMode = false
        }
        break
      }
      case Op.Chain: {
        nodesStack = nodesStack.slice(
          0,
          parIndexStack[parIndexStack.length - 1] + 1
        )
        currentNodes.prog = nodesStack[nodesStack.length - 1]
        branchLeft(currentNodes.prog)
        currentNodes.progSide = NodeSide.L
        break
      }
      case SEPARATOR_REGEX.test(chr) && chr: {
        if (currentNodes.prog.l !== '') {
          currentNodes.progSide = NodeSide.R
        }
        break
      }
      default: {
        if (currentNodes.progSide === NodeSide.R) {
          currentNodes.progSide = NodeSide.L
          currentNodes.prog = branchRight(currentNodes.prog)
          nodesStack.push(currentNodes.prog)
        }

        addChrInProgNode(currentNodes, chr)

        if (chr === Op.LazyNodeStart) {
          currentNodes.progSide = NodeSide.R
        }

        bodyOpen = /[`{]/.test(chr)
        quotesOpen = chr === Op.Quote
        break
      }
    }
  }

  if (typeof currentNodes.text.l === 'string') {
    currentNodes.text.l += Op.BodyEnd
  }

  return tree
}
