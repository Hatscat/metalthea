import type {
  Token,
  Atom,
  TreeNode,
  NodeProduct,
  ExecutionContext,
  ContextFunction,
} from './data'
import * as grammar from './grammar'

export type EvalProduct = {
  result: Atom[]
  errorLogs: string[]
}

function atom2NodeProduct(atom: Atom | null): NodeProduct {
  return atom == null ? null : [atom]
}

function nodeProduct2Atom(nodeProduct: NodeProduct): Atom | null {
  if (nodeProduct == null) return null
  return Array.isArray(nodeProduct) ? nodeProduct[0] : nodeProduct
}

/**
 * Interpret a tree node recursively (postorder tree traversal algorithm).
 * @param {TreeNode} node The tree node to eval.
 * @param {ExecutionContext} execCtx The execution context to execute function in nodes tree.
 * @returns {EvalProduct} The node evaluation result.
 */
export function evalTree(
  tree: TreeNode,
  execContext: ExecutionContext<any> = {}
): EvalProduct {
  const errorLogs: string[] = []

  const token2Atom = (token: Token): Atom | null => {
    if (typeof token !== 'string' || token === '') return null
    if (grammar.BODY.test(token)) {
      const escMap = {
        n: '\n',
        r: '\r',
        t: '\t',
      }
      return token
        .slice(1, -1)
        .replace(/\\([()`{}])/g, '$1')
        .replace(
          /\\([nrt])/g,
          (_string, chr: keyof typeof escMap) => escMap[chr] || `\\${chr}`
        )
    }
    if (grammar.NUMBER_B10.test(token)) return Number(token)
    if (grammar.NUMBER_B16.test(token)) return token
    if (grammar.BOOL.test(token)) return token === 'true'
    if (grammar.KEY.test(token)) return token.slice(1)
    if (grammar.STRING.test(token)) return token.replace(/\\"/g, '"')
    if (grammar.LAZY_NODE.test(token)) return grammar.LAZY_NODE
    if (grammar.OR_NODE.test(token)) return grammar.OR_NODE
    if (grammar.FUNCTION.test(token)) {
      const maybeFunction = token
        .split('.')
        .reduce(
          (context: ExecutionContext | ContextFunction | null, key: string) =>
            context && key in context
              ? (context as ExecutionContext<any>)[key]
              : null,
          execContext
        ) as ContextFunction | null

      if (maybeFunction) return maybeFunction
      errorLogs.push(`Unknown function "${token}".`)
      return null
    }
    errorLogs.push(`Unknown Atom format, token: "${token}".`)
    return null
  }

  const evalNode = (
    node: TreeNode | null,
    nodeContext: Record<string, unknown>,
    extraArguments?: NodeProduct
  ): NodeProduct => {
    if (node == null) return null

    let left: NodeProduct = null
    if (typeof node.l === 'string') {
      const leftAtom = token2Atom(node.l)

      if (leftAtom === grammar.LAZY_NODE) {
        return node.r == null
          ? null
          : atom2NodeProduct((...arguments_: Atom[]) =>
              nodeProduct2Atom(
                evalNode(node.r, Object.assign({}, nodeContext), arguments_)
              )
            )
      }

      left = atom2NodeProduct(leftAtom)
    } else if (node.l != null && node.l.l != null) {
      left = evalNode(node.l, nodeContext)
    }

    let right: NodeProduct = null
    if (node.r != null && node.r.l != null) {
      right = evalNode(node.r, Object.assign({}, nodeContext), extraArguments)
    } else if (extraArguments) {
      right = extraArguments
    }

    if (left == null) return right

    const leftHead: Atom = left[0]

    if (left.length > 1) {
      const leftTail = left.slice(1)
      // eslint-disable-next-line unicorn/prefer-spread
      right = right == null ? leftTail : leftTail.concat(right)
    }

    if (leftHead === grammar.OR_NODE) {
      return right != null && right.length > 1 ? right.slice(0, -1) : right
    }

    if (typeof leftHead === 'function') {
      try {
        return atom2NodeProduct(leftHead.apply(nodeContext, right ?? []))
      } catch (error) {
        errorLogs.push(
          `Error with function "${
            typeof node.l === 'string' ? node.l : ''
          }": ${error}`
        )
        return null
      }
    }

    if (right == null) return [leftHead]
    return [leftHead, ...right]
  }

  const treeResult = evalNode(tree, {})

  return {
    result: treeResult || [],
    errorLogs,
  }
}
