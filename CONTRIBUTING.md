# Contributing documentation

## Project resources

### Issues

[Project issues are tracked there](https://gitlab.com/Hatscat/metalthea/issues)

### Continuous Integration

You can check [CI/CD pipelines there](https://gitlab.com/Hatscat/metalthea/pipelines)

## Development environment setup

* fork this repository,
* run `yarn` from it.

### Editor configuration

* [Visual Studio Code recommended plugins](.vscode/extensions.json)

## Guidelines

* Code should be clean and typed (with TypeScript).
* Tests should be present.
* Comments could be added when you need to clarify a design decision or assumptions about the spec.
* Documentation could be added to clarify any details (how to run tests, etc).

## Commands

```sh
$ yarn test # run tests with Jest
$ yarn test:integration # build, run the cli program and compare output with expected output
$ yarn coverage # run tests with coverage and open it on browser
$ yarn scan # check code (lint + types)
$ yarn docs # generate docs
$ yarn build # generate docs and transpile code
$ yarn validate # scan checks + tests with coverage
```

## Project Structure
```sh
src/
├── cli.js # program entry point
├── index.js # api entry point
├── grammar.js # grammar regex constants
├── parser.js # program string parser to abstract nodes tree
└── interpreter.js # abstract nodes tree evaluator and atom converter
tests/ # unit and integration tests
├── integration/ # integration tests
```

## Control Flow

![model](http://www.plantuml.com/plantuml/svg/VPB1Zk8m38RlUGeVGMBt0QYqOjLXZWZDp9M3IKs9Q9FASQ4ZvU772LNdneKazkV_vs-oYu8iOz-PP3kNJ3cQF_y0kOrWPJgYN3SGsJutedMIlA_C9V3D02WySvMUjcqNpkVlK-a2jkXyOMQ9byoilc0ztkZDyScGRw2uRMte1zVHQbsPxIN-PSBcNS_99ESRcj92V8QHBTMXAMmQa2DnoicYiFCjn0LGgT8-ScWP-ymhziL41-q2ddK2hUH2zSAdz8HuO9BPQj4GtMnWFv4TnGLV1mKd0T92BPF-0RfZb-bi_hYgBgo642LmGroczGVln16xjG4e_64E1_2RIiADV9K96xpoybz2lq2zcwW1r5ALlsanCbhVgvgcRqOhC3pNrqbrfYvLp74-A8wT09UbcbzIlfu7Ew5yFoUZMGQpKrDzQpy0)

## [Types](src/types.flow.js)

## [API](API.md)
